
										<!-- Header -->
											<?php
												include("includes/header.php");
											?>
										<!-- Header / End -->


									<!-- Content
================================================== -->
									<div class="container">
										<div class="row">
											<form  enctype="multipart/form-data" method="POST">
												<!-- Submit Page -->
												<div class="col-md-12">
													<div class="submit-page">
													<!-- Section -->
														<h3>Basic Information</h3>
														<div class="submit-section">
															<!-- Title -->
															<div class="form">
																<h5>Art Title <i class="tip" data-tip-content="Type title that will also contains an unique feature of your property (e.g. renovated, air contidioned)"></i></h5>
																<input type='text' name='name' id="title"/>
															</div>

															<h5>Images</h5>

															<div class="dropzone" id="myDropzone">
																<div class="fallback">
																	<input name="file" type="file" multiple />
																</div>
															</div>


															<!-- Row -->
															<div class="row with-forms">

																<!-- Type -->
																<div class="col-md-6">
																	<h5>Type</h5>
																	<select  name="type" id="type">
																		<?php
									
										require('includes/config.php');
			
											$query="select * from category ";
			
											$res=mysqli_query($conn,$query);
											
											while($row=mysqli_fetch_assoc($res))
											{
												echo "<option disabled>".$row['cat_nm'];
												
												$q2 = "select * from subcat where parent_id = ".$row['cat_id'];
												
												$res2 = mysqli_query($conn,$q2) or die("Can't Execute Query..");
												while($row2 = mysqli_fetch_assoc($res2))
												{	
												
										echo '<option value="'.$row2['subcat_id'].'"> - '.$row2['subcat_nm'];
												
													
												}
												
											}
										//	mysqli_close($link);
								?>
																	</select>
																</div>

																<!-- Type -->
																<div class="col-md-6">
																	<h5>Price <i class="tip" data-tip-content="Type overall or monthly price if property is for rent"></i></h5>
																	<div class="select-input disabled-first-option">
																		<input type='text' name='price' id='price' data-unit='USD' >
																	</div>
																</div>

															</div>
																<!-- Row / End -->
																<!-- Row -->
															<div class="row with-forms">
																<?php
$query="select * from user where u_id= ".$_SESSION['uid'];
			
$user = mysqli_fetch_assoc(mysqli_query($conn,$query));																
																if ($_SESSION['unm']!="admin"){ ?>
																					<!-- artist -->
																					<div class="col-md-6">
																						<h5>Artist </h5>
																						<input type='text' name='publisher' value = "<?php echo $user['u_fnm']; ?>"size='40' id='Artist'>
																						</div>
																<?php }else {?>

																						<!-- Zip-Code -->
																						<div class="col-md-6">
																							<h5>Artist</h5>
																							<input type='text' name='publisher' size='40' id='users'>
																							</div>

																						
																<?php } ?>
																</div>
																						<!-- Row / End -->
																<!-- Description -->
																<div class="form">
																	<h5>Description</h5>
																	<textarea class="WYSIWYG" id ='description' name='description' cols="40" rows="3" spellcheck="true"></textarea>
																</div>
																

														</div>
															<!-- Section / End -->

															


																					</div>
																					<!-- Section / End -->

																					

																					</div>
																				</div>

																			</div>
<div class="col-md-6">
																						
																						<button type="submit" id="submit-all" class="margin-top-5 btn btn-primary btn-xs">Upload the file</button>
																						</div>
																		</form>
																		


																		<!-- Section -->

																		<!-- Section / End -->
																	</div>
																</div>
																<!--
</div>
</div>
 Footer
================================================== -->
																<div class="margin-top-55"></div>
																
															<script type="text/javascript" src="scripts/dropzone.js"></script>
															<script>
																
																Dropzone.options.myDropzone= {
																url: 'process_addproperty.php',
																paramName: 'img',
																autoProcessQueue: false,
																uploadMultiple: true,
																parallelUploads: 5,
																maxFiles: 6,
																//maxFilesize: 1,
																acceptedFiles: 'image/*',//".png,.jpg,.gif,.bmp,.jpeg"
																addRemoveLinks: true,
																init: function() {
																dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

																// for Dropzone to process the queue (instead of default form behavior):
																document.getElementById("submit-all").addEventListener("click", function(e) {
																// Make sure that the form isn't actually being sent.
																e.preventDefault();
																e.stopPropagation();
																dzClosure.processQueue();
																});

																//send all the form data along with the files:
																this.on("sendingmultiple", function(data, xhr, formData) {
																formData.append("title", jQuery("#title").val());
																formData.append("description", jQuery("#description").val());
																formData.append("price", jQuery("#price").val());
																formData.append("publisher", jQuery("[name = publisher]").val());
																formData.append("cat", jQuery("#type").val());
																
																});
																},
																success: function(file, response){
																	var res = JSON.parse(response);
																	window.location = res.urllink;
    }
																}
															</script>

																<?php
							include("includes/footer.php");
						?>
																