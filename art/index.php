<!-- Header -->
<?php	include("includes/header.php");	?>
	<!-- Header / End -->
<!-- Banner
================================================== -->
<div class="parallax" data-background="images/home-parallax.jpg" data-color="#36383e" data-color-opacity="0.5" data-img-width="2500" data-img-height="1600">

	<div class="container">
		<div class="row">
			
            <?php
							include("includes/banner.php");
						?>
            
		</div>
	</div>

</div>

		
<!-- Content
================================================== -->


<!-- Featured -->
<div class="container">
	<div class="row">
	
		<div class="col-md-12">
			<h3 class="headline margin-bottom-25 margin-top-65">Newly Added</h3>
		</div>
		
		<!-- Carousel -->
		<?php
							include("includes/featured.php");
						?>
		<!-- Carousel / End -->

	</div>
</div>
<!-- Featured / End -->


<!-- Flip banner -->
<a href="listings-half-map-grid-standard.html" class="flip-banner parallax" data-background="images/flip-bg.jpg" data-color="#274abb" data-color-opacity="0.7" data-img-width="2500" data-img-height="1600">
	<div class="flip-banner-content">
		<h2 class="flip-visible">We help people and art find each other</h2>
		<h2 class="flip-hidden">Browse Gallery <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
<!-- Flip banner / End -->



<!-- Footer
================================================== -->
<?php
							include("includes/footer.php");
						?>
