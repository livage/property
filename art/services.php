
	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->


<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row fullwidth-layout">

		<div class="col-md-12">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					<div class="sort-by">
						<h1 class="cushycms">Our Services</hl>

						
					</div>
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container list-layout">

<div class="slider-wrapper theme-default" class="cushycms">
               <div id="nivoslider" class="nivoSlider">
                  <a href="#"><img src="images/pages4.jpg" ></a>
                   <a href="#"><img src="images/pages5.jpg" ></a>
                  <a href="#"><img src="images/pages6.jpg"></a>
               </div>
            </div><br>
            
            
            
<h3 class="cushycms">Property Sales</h3>
<p class="cushycms">• Listing and selling properties on behalf of property owners.<br>
• Providing our clients with a network of buyers and sellers.<br>
• Free Comparative Market Analysis.<br>
• Advertising in local real estate publications for the benefit of our clients.<br>
• Attractive yard signs with flyer tubes attached for potential buyers driving by<br>
• Circulated open house invitations (per seller approval).<br>
• Excellent follow-up system on each showing.<br>
• If no contract is submitted within the first listed month, we re-analyze our future marketing procedures with the seller.<br>
• Inside/outside photos in panoramic view placed on real estate Internet sites.<br>
This allows potential buyers to view our seller's property online.
Buyers will be provided with the following services:<br>
• Matching - We match buyers to homes. We have an extensive questionnaire for our buyers to list their wants and needs. We then take this questionnaire and put the supplied information into our program to match buyers to the homes matching their criteria.<br>
• Site Visiting - At Property Exchange we take a step further by going on site for our clients. We visit the properties of their choice and take inside and outside panoramic photos for their viewing. We then email the photos or have a face- to face showing with the buyer. The buyer does not have to go on-site unless they choose to.<br>
Negotiating - On behalf of our clients  </p>


<h3 class="cushycms">Property Management</h3>
<p class="cushycms">• Negotiate / approve rental / lease of various properties in a portfolio on behalf of property owners and ensure that terms of lease agreement are adhered to timeously.<br>
• Handle the financial operations of the property, ensuring that rent is collected and that mortgages, taxes, insurance premiums, payroll, and maintenance bills are paid on time.<br>
• Market vacant space to prospective tenants through advertising or
alternative networks, and establish rental rates in line with market trends.<br>
• Review portfolio periodically and identify properties that are no longer financially profitable. Negotiate the sale of, or terminate the lease on, such properties.<br>
• Oversee the performance of income-generating commercial or
residential properties and ensure that real estate investments achieve
their expected revenues.<br>
• Inspect properties regularly and make sure problems are corrected and
appropriate repairs and improvements are carried out Visiting the
properties (sometimes on a daily basis) when contractors are doing \
major repair or renovation work<br>
• Prepare and administer contracts for provision of property services, such as cleaning and maintenance, security services and alarm systems.<br>
• Co-ordinate the implementation of repairs, maintenance and renovations carried out on buildings and monitor progress and cost of work for property owners.</p>
<h3 class="cushycms">Property Valuation</h3>
<p class="cushycms">Valuations for all types of immovable and movable properties for-<br>
• Sellers<br>
• Purchasers<br>
• Rental valuations<br>
• Insurance<br>
• Mortgages<br>
• Deceased Estates<br>
• Accounting<br>
• Statutory requirements</p>

<h3 class="cushycms">We Serve:</h3>
<p class="cushycms">• Businesses and individuals looking for buyers of their property:<br>
• Businesses and individuals looking for properties to buy;<br>
• Businesses intending to find offices to lease in the CBD or in other areas;<br>
• Individuals looking for accommodation on term lease basis;<br>
• Property owners looking for property managers and consultants to administer their properties and offer them hassle free maintenance and
payment schedules.<br>
• Organisations in need of property valuations for capital transactions or for balance sheet extractions;<br>
• Property developers looking for project managers.<br>
• Individuals or organisations looking for auctioneers;<br><br>
We provide superior personal services to buyers and sellers. We manage
properties professionally and ensure that clients get a good return on their investments. </p>
<h3 class="cushycms">Property Management</h3>
<p class="cushycms">We have a strategic alliance with a reputable architects and engineers who have been involved in various large projects within and outside Zimbabwe.This will see our company offering the following services in partnership with them-<br>
• project feasibility studies<br>
• project funding, structuring and mobilisation<br>
• design and planning consultancy<br>
• procurement services<br>
• development/project management<br>
• project engineering services<br>
• construction management service</p>
<h3 class="cushycms">Auctioneering</h3>
<p class="cushycms">• house clearance sales on or off your premises<br>
• vehicle auctions<br>
• plant and machinery auctions<br>
• adequate advertising to attract bidders<br>
• choice of convenient places for potential bidders value assessments</p>

			</div>
			<!-- Listings Container / End -->

			<div class="clearfix"></div>
			<!-- Pagination -->
			
			<!-- Pagination / End -->

		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
