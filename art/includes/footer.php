<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<!--<img class="footer-logo" src="images/official logo.jpg" alt="">-->
				<br><br>
				<p>Our mission is to be the leaders in Fine Art marketing through excellent service and provision of a diverse range of Arts to the satisfaction of all our curators</p>
		  </div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				<ul class="footer-links">
					<li><a href="login-register.php">Login</a></li>
					<li><a href="login-register2.php">Sign Up</a></li>
					<li><a href="login-register.php">My Account</a></li>
					
					
				</ul>

				<ul class="footer-links">
					<li><a href="faqs.php">FAQ</a></li>
					
					<li><a href="contact1.php">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>Mabvuku, Harare</span> <br>
					Phone: <span>+263  </span><br>
					E-Mail:<span> <a href="mailto:sales@Cavemansolutions.ca">sales@Cavemansolutions.ca</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="https://www.facebook.com/www.propertyexchange.co.zw/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/propertyexchnge" target="_blank"><i class="icon-twitter"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/propertyexchangezimbabwe/?hl=en" target="_blank"><i class="icon-instagram"></i></a></li>
                    
                     <li><a class="youtube" href="#" target="_blank"><i class="icon-youtube"></i></a></li>
                   
					
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2019 Caveman Solutions. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
 <script src="js/jquery.nivo.slider.pack.js"></script>
  <script src="js/imagesloaded.pkgd.min.js"></script>
  <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="js/jquery.parallax.js"></script>
      <script src="js/jquery.wait.js"></script> 
      <script src="js/modernizr-2.6.2.min.js"></script> 
      <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
      <script src="js/jquery.nivo.slider.pack.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/superfish.js"></script>
      <script src="js/tweetMachine.js"></script>
      <script src="js/tytabs.js"></script>
      <script src="js/jquery.gmap.min.js"></script>
      <script src="js/circularnav.js"></script>
      <script src="js/jquery.sticky.js"></script>
      <script src="js/jflickrfeed.js"></script>
      <script src="js/imagesloaded.pkgd.min.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script src="js/spectrum.js"></script>
      <script src="js/switcher.js"></script>
 <!--     <script src="js/custom.js"></script>


 Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>

<script type="text/javascript" src="jquery.js"></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />


<script type="text/javascript">
$().ready(function() {
									$("#users").autocomplete("get_users.php", {

									width: 260,
									matchContains: true,
									mustMatch: true,
									minChars: 1,
									//multiple: true,
									highlight: false,
									//multipleSeparator: ",",
									selectFirst: false
									});
									
									$("#suburb").autocomplete("get_course_list.php" , {
									extraParams: {
									region:function() { return $('#region').val();}
									},
									width: 260,
									matchContains: true,
									mustMatch: true,
									minChars: 0,
									//	multiple: true,
									highlight: false,
									//multipleSeparator: ",",
									selectFirst: false
									});
									
});
</script>

<!-- Style Switcher / End -->





</div>
<!-- Wrapper / End -->


</body>


</html>