
	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->




<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row fullwidth-layout">

		<div class="col-md-12">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					<div class="sort-by">
						<h1>About Us</hl>

						
					</div>
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container list-layout">


<div class="slider-wrapper theme-default cushycms">
               <div id="nivoslider" class="nivoSlider">
                  <a href="#"><img src="images/pages.jpg" ></a>
                   <a href="#"><img src="images/pages2.jpg" ></a>
                  <a href="#"><img src="images/pages3.jpg"></a>
               </div>
            </div>
            
            
            
            
            
<h3 class="cushycms">Vision</h3>
<p class="cushycms">To become the prime and most preferred online Art dealer.  </p>
<h3 class="cushycms">Mission</h3>
<p class="cushycms">To be the leaders in the Art marketing through excellent service and provision of a diverse range of Art products and services to the satisfaction of all our stakeholders</p>
<h3 class="cushycms">Our Values</h3>
<p class="cushycms">• Integrity<br>
• Professionalism<br>
• Excellence<br>
• Honesty<br>
• Trust<br>
• Creativity<br>
• Teamwork</p>

<h3 class="cushycms">We Serve:</h3>
<p class="cushycms">• Artist looking for buyers of their Art:<br>
• Businesses and individuals looking for Art to buy;<br>
• Artist looking for hassle free marketing and selling Platform.<br>
• Organisations in need of property valuations for capital transactions or for balance sheet extractions;<br>
• Property developers looking for project managers.<br>
• Individuals or organisations looking for auctioneers;<br><br>
We provide superior services to buyers and sellers.  </p>

			</div>
			<!-- Listings Container / End -->

			<div class="clearfix"></div>
			<!-- Pagination -->
			
			<!-- Pagination / End -->

		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>


<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->

