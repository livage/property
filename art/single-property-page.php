
	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->


<?php
require('includes/config.php');

$query = "SELECT * FROM book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id  where b_id=".$_GET['id'];
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);



//var_dump($row);
?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="property-titlebar margin-bottom-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<a href="#" class="back-to-listings"></a>
				<div class="property-title">
					<h2><?php echo $row['b_nm'];?> <span class="property-badge"><?php echo $row['subcat_nm'];?></span></h2>
					<span>
						<a href="#location" class="listing-address">
							
							<?php  echo 'Artist: '.ucwords($row['b_publisher']);?>
						</a>
					</span>
				</div>

				<div class="property-pricing">
					<div>$<?php echo $row["b_price"]; if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
						echo ' monthly';
						?></div>
					
				</div>


			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row margin-bottom-50">
		<div class="col-md-12">
		
			<!-- Slider Container -->
			<div class="property-slider-container">

				<!-- Agent Widget -->
				<div class="agent-widget">
					<div class="agent-title">
						<div class="agent-photo"><img src="images/agent-avatar.jpg" alt="" /></div>
						<div class="agent-details">
							<h4><a href="#"><?php echo $row['b_user'];?></a></h4>
							<span><i class="sl sl-icon-call-in"></i><?php echo $row['b_contact'];?></span>
						</div>
						<div class="clearfix"></div>
					</div>
                    <div id="contact-message"></div> 
<form method="post" action="contact.php" name="contactform" id="contactform" autocomplete="on">
<input name="subject" type="text" id="subject" placeholder="Your Name" required />
					<input name="email" type="email" id="email" placeholder="Email Address" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required />
					<input type="text" name="name" id="name" placeholder="Your Phone">
					<textarea name="comments" id="comments">I'm interested in this piece titled <?php echo $row['b_nm'];?>  and I'd like to know more details.</textarea>
					<button class="button fullwidth margin-top-5">Send Message</button>
                    </form>
				</div>
				<!-- Agent Widget / End -->

				<!-- Slider -->
				<div class="property-slider no-arrows">
					<a href="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" class="item mfp-gallery"></a>
					<?php if (!empty($row['b_img2'])){
					?>
					<a href="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" class="item mfp-gallery"></a>
                    
					<?php }?>
                    
                    
					<!--<a href="images/single-property-03.jpg" data-background-image="images/single-property-03.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-04.jpg" data-background-image="images/single-property-04.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-05.jpg" data-background-image="images/single-property-05.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-06.jpg" data-background-image="images/single-property-06.jpg" class="item mfp-gallery"></a>-->
				</div>
				<!-- Slider / End -->

			</div>
			<!-- Slider Container / End -->

			<!-- Slider Thumbs -->
			<div class="property-slider-nav">
				<div class="item"><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt=""></div>
				<?php if (!empty($row['b_img2'])){
				?>
				<div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" alt=""></div>
                
				<?php }?>
                
			<!--	<div class="item"><img src="images/single-property-02.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-03.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-04.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-05.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-06.jpg" alt=""></div>-->
			</div>

		</div>
	</div>
</div>


<div class="container">
	<div class="row">

		<!-- Property Description -->
		<div class="col-lg-8 col-md-7">
			<div class="property-description">

			

				<!-- Description -->
				<h3 class="desc-headline">Description</h3>
				<div>
					<?php echo $row['b_desc'];?>
					
				</div>

				<!-- Details -->
				


			

			

			</div>
		</div>
		<!-- Property Description / End -->


		<!-- Sidebar -->
		<div class="col-lg-4 col-md-5">
			<div class="sidebar sticky right">

				<!-- Widget -->
				<div class="widget margin-bottom-35">
					<button class="widget-button"><i class="sl sl-icon-printer"></i> Print</button>
					<button class="widget-button save" data-save-title="Save" data-saved-title="Saved"><span class="like-icon"></span></button>
				</div>
				<!-- Widget / End -->



				</div>
				<!-- Widget / End -->

			

			</div>
		</div>
		<!-- Sidebar / End -->

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->
