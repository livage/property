<?php
require('includes/config.php');
 session_start();


	//$cat=$_GET['cat_nm'];
	
	$q = "select * from subcat where parent_id = ".$_GET['cat'];
	$res = mysqli_query($conn,$q) or die("Can't Execute Query..");
	
	$row1 = mysqli_fetch_assoc($res);
	
	if($_GET['catnm']==$row1['subcat_nm'])
	{
		header("location:booklist.php?subcatid=".$row1['subcat_id']."&subcatnm=".$row1["subcat_nm"]);
		
	}

include("includes/header.php");
?>
	<!-- Header / End -->



<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
    
<h3 class="margin-top-0 margin-bottom-35"><?php echo $_GET['catnm'];?></h3>

<div class="slider-wrapper theme-default">
               <div id="nivoslider" class="nivoSlider">
                  <a href="#"><img src="images/pages.jpg" ></a>
                   <a href="#"><img src="images/pages2.jpg" ></a>
                  <a href="#"><img src="images/pages3.jpg"></a>
               </div>
            </div><br><br>
		<div class="col-md-8">
        
<form autocomplete="off" action="search_results.php" method="post">
        <div class="main-search-input margin-bottom-35">
				<input type="text"  id="search" name="course" class="ico-01" placeholder="Enter address e.g. suburb and city " value=""/>
				<button class="button">Search</button>
			</div>
				</form>
			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="list"><i class="fa fa-th-list"></i></a>
						<a href="#" class="grid"><i class="fa fa-th-large"></i></a>
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container grid-layout">

				<?php
				require('includes/config.php');

				$sqlfilter="1";

				if (isset($_POST['course']) && $_POST['course'] != "" )
					$sqlfilter .=" AND b_publisher = '".$_POST['course']."'";


				//if (isset($_POST['type']) && $_POST['type'] != "")
					$sqlfilter .=" AND cat_nm = '".$_GET['catnm']."'";

				if (isset($_POST['status']) && $_POST['status'] != "")
					$sqlfilter .=" AND subcat_nm = '".$_POST['status']."'";

				if (isset($_POST['price']) && $_POST['price'] != "")
					$sqlfilter .=" AND b_price  BETWEEN 0 AND ".$_POST['price'];


				$query="select * from book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id where ".$sqlfilter."  ORDER BY b_id DESC ";

				$res=mysqli_query($conn,$query);

				while($row=mysqli_fetch_assoc($res)) {
					//echo "<option>".$row['cat_nm'];
					//var_dump($row);
					?>


					<!-- Listing Item -->
					<div class="listing-item">

						<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="listing-img-container">

							<div class="listing-badges">
								<?php if ($row['b_feature']==1){?>
									<span class="featured">Featured</span>
								<?php }?>
								<span><?php echo $row['subcat_nm'];?></span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$<?php echo $row['b_price']; if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
										echo ' monthly';
									?> <i>$<?php echo number_format((float)$row['b_price']/$row['b_area'], 2, '.', '');?> / sq ft</i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt=""></div>
								<?php
								if (!empty($row['b_img2'])){
									?>
									<div><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>
								<?php }?>
							</div>
						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="#"><?php echo $row['b_nm'];?></a></h4>
								<a href="https://maps.google.com/maps?q=<?php echo $row['b_address'].',+'.$row['b_edition'].',+'. $row['b_publisher'];?>,+zimbabwe"
								   class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									<?php  echo $row['b_address'].', '.$row['b_edition'].', '.$row['b_publisher'];?>
								</a>

								<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="details button border">Details</a>
							</div>

							<ul class="listing-details">
								<li><?php echo $row['b_area'];?> sq ft</li>
								<li><?php echo $row['b_isbn'];?> Bedroom</li>
								<li><?php echo $row['b_rooms'];?> Rooms</li>
								<li><?php echo $row['b_page'];?> Bathroom</li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> <?php echo $row['b_user']?></a>
								<span><i class="fa fa-calendar-o"></i> <?php
									$zuva=explode(' ',$row['b_date']);
									list($yy,$mm,$dd)=explode('-',$zuva[0]);

									$start=date($yy.'-'.$mm.'-'.$dd);
									$end =date("Y-m-d");


									$tZone = new DateTimeZone('GMT');
									$dt1 = new DateTime($start, $tZone);
									$dt2 = new DateTime($end, $tZone);
									$ts1 = $dt1->format('Y-m-d');
									$ts2 = $dt2->format('Y-m-d');
									$diff = abs(strtotime($ts1)-strtotime($ts2));
									$diff/= 3600*24;

									echo $diff." days ago";
									?></span>
							</div>

						</div>

					</div>

					<?php
				}
					?>
			</div>
			<!-- Listings Container / End -->

			
			<!-- Pagination -->
			
			<!-- Pagination / End -->

		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-md-4">
			<div class="sidebar sticky right">

				<!-- Widget -->
				<div class="widget margin-bottom-40">

 <p><!-- Currency Converter Script - EXCHANGERATEWIDGET.COM -->
<div style="width:270px;border:1px solid #55A516;"><div style="text-align:center;background-color:#55A516;width:270px;height:400px;font-size:13px;font-weight:bold;height:25px;padding-top:2px;"><a href="#" style="color:#FFFFFF;text-decoration:none;" rel="nofollow">Currency Converter</a></div><script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=USD&t=EUR&a=1&d=F0F0F0&n=FFFFFF&o=000000&v=1"></script></div>
<!-- End of Currency Converter Script --></p>

<br><br>

                <div style="width:270px; text-align:center;"><p style="background-color:#F4F7F9;"><a href="http://www.mortgagecalculator.org/" target="_blank"><img src="http://www.mortgagecalculator.org/free-tools/calculator/mortgage-calculator-logo.png" alt="Property Exchange"></a><br><iframe src="http://www.mortgagecalculator.org/free-tools/calculator/caller.html" frameborder="0" width="270px" height="400" scrolling="no"></iframe><br><a href="http://www.mortgagecalculator.org/free-tools/"><font color="#000000"> </font></a></p></div>
									
                                    

					
                   
					</div>

				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->
	</div>
	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>


